const sonarqubeScanner = require('sonarqube-scanner')
sonarqubeScanner(
  {
    serverUrl: 'http://localhost:9000',
    options: {
      'sonar.sources': 'backend',
      'sonar.tests': 'backend',
      'sonar.inclusions': 'backend/**',
      'sonar.test.inclusions': 'backend/**/**.test.ts',
      'sonar.typescript.lcov.reportPaths': '**/coverage/lcov.info',
      'sonar.login': 'admin',
      'sonar.password': 'admin123'
    },
  },
  () => {}
)
