import { Router } from 'express'
import {
  createBook,
  getAllBooks,
  healthy,
} from '../controllers/book.controller'
import validator from '../middlewares/validator'

const router = Router()

router.get('/ping', healthy)
router.post('/', [validator], createBook)
router.get('/', getAllBooks)

export default router
