import { Request, Response } from 'express'
import { QueryTypes } from 'sequelize'
import db from '../config/database'
import client from '../config/redis'

export const healthy = (req: Request, res: Response) => {
  global.log.info('POST /product')
  return res.status(200).json({ msg: 'true' })
}

export const createBook = async (req: Request, res: Response) => {
  try {
    const { name, description, price, image, author } = req.body

    const query =
      'insert into book(name, description, price, image, author) values (:name, :description, :price, :image, :author) RETURNING id;'

    const [data] = await db.query(query, {
      replacements: {
        name: name,
        description: description,
        price: price,
        image: image,
        author: author,
      },
      type: QueryTypes.SELECT,
    })

    global.log.info('POST /product')

    return res.status(201).json({ book: data })
  } catch (error) {
    if (error instanceof Error) {
      global.log.error(`Create Book: ${error.message}`)
      return res.status(500).json({ error: error.message })
    }
  }
}

export const getAllBooks = async (req: Request, res: Response) => {
  try {
    let books = await client.get('books2')

    if (books) {
      global.log.info('Data desde redis')
      return res.status(200).json({
        books: JSON.parse(books),
      })
    }

    const query = 'select * from book'

    const data = await db.query(query, {
      type: QueryTypes.SELECT,
    })

    await client.set('books2', JSON.stringify(data))

    global.log.info('GET /product de la db')

    return res.status(200).json({ books: data })
  } catch (error) {
    if (error instanceof Error) {
      global.log.error(`View Books: ${error.message}`)
      return res.status(500).json({ error: error.message })
    }
  }
}
