import { Request, Response } from 'express'
import {
  createBook,
  getAllBooks,
  healthy,
} from '../controllers/book.controller'

import db from '../config/database'
import client from '../config/redis'

import '../logger/logger'
import { cli } from 'winston/lib/winston/config'

beforeAll(() => {
  global.log.info = jest.fn()
  global.log.error = jest.fn()
  global.log.debug = jest.fn()
})

jest.mock('../config/redis', () => ({
  connect: jest.requireActual('redis-mock'),
  createClient: jest.fn().mockReturnValue({
    on: jest.fn(),
    quit: jest.fn(),
    get: jest.fn(),
    set: jest.fn(),
  }),
}))

describe('healthy', () => {
  let mockRequest: Partial<Request>
  let mockResponse: Partial<Response>
  let responseObject: any

  beforeEach(() => {
    mockRequest = {}
    mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn().mockImplementation(result => {
        responseObject = result
      }),
    }
  })

  test('200 - healthy ', () => {
    const expectedStatusCode = 200
    const expectedResponse = { msg: 'true' }

    healthy(mockRequest as Request, mockResponse as Response)

    expect(mockResponse.status).toHaveBeenCalledWith(expectedStatusCode)
    expect(responseObject).toEqual(expectedResponse)
  })
})

describe('createBook', () => {
  let mockRequest: Partial<Request>
  let mockResponse: Partial<Response>
  let mockQuery
  let responseObject: any
  const data = {
    name: 'test',
    description: 'description test',
    price: 'price test',
    image: 'image test',
    author: 'author test',
  }

  beforeEach(() => {
    mockRequest = {
      body: data,
    }
    mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn().mockImplementation(result => {
        responseObject = result
      }),
    }

    mockQuery = jest.fn().mockResolvedValue([data])
    db.query = mockQuery
  })

  test('200 - createBook', async () => {
    const expectedStatusCode = 201
    const expectedResponse = { book: data }

    await createBook(mockRequest as Request, mockResponse as Response)

    expect(mockResponse.status).toHaveBeenCalledWith(expectedStatusCode)
    expect(responseObject).toEqual(expectedResponse)
  })

  test('500 - createBook ', async () => {
    const expectedStatusCode = 500
    // @ts-ignore
    db.query.mockImplementation(() => {
      throw new Error('Sequelize error')
    })

    await createBook(mockRequest as Request, mockResponse as Response)

    expect(mockResponse.status).toHaveBeenCalledWith(expectedStatusCode)
  })
})

describe('getAllBooks', () => {
  let mockRequest: Partial<Request>
  let mockResponse: Partial<Response>
  let responseObject: any

  beforeEach(() => {
    mockRequest = {}
    mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn().mockImplementation(result => {
        responseObject = result
      }),
    }

    client.get = jest.fn().mockReturnValue('[]')
  })

  test('200 - getAllBooks -  redis', async () => {
    const expectedStatusCode = 200
    const expectedResponse = { books: [] }

    await getAllBooks(mockRequest as Request, mockResponse as Response)

    expect(mockResponse.status).toHaveBeenCalledWith(expectedStatusCode)
    expect(responseObject).toEqual(expectedResponse)
  })
})

describe('getAllBooks', () => {
  let mockRequest: Partial<Request>
  let mockResponse: Partial<Response>
  let responseObject: any

  beforeEach(() => {
    mockRequest = {}
    mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn().mockImplementation(result => {
        responseObject = result
      }),
    }
    client.set = jest.fn()
    db.query = jest.fn().mockReturnValue([])
  })

  test('200 - getAllBooks - DB', async () => {
    const expectedStatusCode = 200
    const expectedResponse = { books: [] }

    await getAllBooks(mockRequest as Request, mockResponse as Response)

    expect(mockResponse.status).toHaveBeenCalledWith(expectedStatusCode)
    expect(responseObject).toEqual(expectedResponse)
  })

  test('500 - getAllBooks - db', async () => {
    const expectedStatusCode = 500

    // @ts-ignore
    db.query.mockImplementation(() => {
      throw new Error('Sequelize error')
    })
    await getAllBooks(mockRequest as Request, mockResponse as Response)

    expect(mockResponse.status).toHaveBeenCalledWith(expectedStatusCode)
  })
})
