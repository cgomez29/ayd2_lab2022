import { createClient } from 'redis'

const REDIS_PORT = process.env.REDIS_PORT
const REDIS_HOST = process.env.REDIS_HOST

const client = createClient({
  url: `redis://${REDIS_HOST}:${REDIS_PORT}`,
})

client.on('error', err => global.log.error(`Redis client error: ${err}`))

client.connect()

export default client
