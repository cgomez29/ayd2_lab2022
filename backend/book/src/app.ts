import './logger/logger'
import express, { Application } from 'express'
import morgan from 'morgan'
import cors from 'cors'
import { serve, setup } from 'swagger-ui-express'

import docs from './docs'
import bookRoutes from './routes/book.routes'

const app: Application = express()

const PORT = process.env.PORT
const SERVER = 'book'

// settings
app.set('port', PORT)

// middlewares
app.use(morgan('dev'))
app.use(cors())
app.use(express.urlencoded({ limit: '5mb', extended: true }))
app.use(express.json({ limit: '10mb' }))

// routes
app.use(`/${SERVER}`, bookRoutes)
app.use(`/${SERVER}/docs`, serve, setup(docs))

export default app
