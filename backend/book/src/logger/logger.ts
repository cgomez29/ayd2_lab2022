import winston, { LoggerOptions } from 'winston'


const { timestamp, combine } = winston.format

const { NODE_ENV, LOG_LEVEL } = {
    NODE_ENV: process.env.NODE_ENV, 
    LOG_LEVEL: process.env.NODE_ENV === 'prod' ? 'info' : 'debug'
}

const logTransport = new winston.transports.File({
    level: LOG_LEVEL,
    filename: 'log/combined.log',
    maxsize: 1024,
    maxFiles: 3,
})

const errorTransport = new winston.transports.File({
    level: 'error',
    filename: 'log/error.log',
    maxsize: 1024,
    maxFiles: 3,
})

const config: LoggerOptions = {
    level: LOG_LEVEL,
    defaultMeta: {
        service: 'Book'
    },
    format: combine(timestamp(), winston.format.json()),
    transports: [logTransport, errorTransport] 
}

const logger = winston.createLogger(config)

if(NODE_ENV !== 'prod') {
    logger.add(
        new winston.transports.Console({
            format: combine(
                winston.format.colorize(),
                winston.format.simple(),
                timestamp()
            )
        })
    )
}


declare global {
    var log: any
}

global.log = logger