import dotenv from 'dotenv'
dotenv.config()

import app from './app'

const PORT = app.get('port')

const main = () => {
  app.listen(PORT, () => console.log(`Running in: http://localhost:${PORT}`))
}

main()
