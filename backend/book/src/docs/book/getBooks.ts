export default {
  get: {
    tags: ['Book CRUD operations'],
    description: 'Get Books',
    operationId: 'getBooks',
    parameters: [],
    responses: {
      200: {
        description: 'Books were obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Book',
            },
          },
        },
      },
    },
  },
}
