import getBooks from './getBooks'
import postBook from './postBook'

export default {
  paths: {
    '/book': {
      ...getBooks,
      ...postBook,
    },
    //'/Book/{id}': {
    //  ...deleteBook,
    //  ...putBook,
    //},
    //'/Book/song/{id}': {
    //  ...getBook,
    //},
  },
}
