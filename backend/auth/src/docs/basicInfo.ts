export default {
  openapi: '3.0.1',
  info: {
    version: '1.0.0',
    title: 'Book',
    description: 'Book API',
    contact: {
      name: 'Cristian Gómez',
      email: 'crisgomez029@gmail.com',
    },
  },
}
