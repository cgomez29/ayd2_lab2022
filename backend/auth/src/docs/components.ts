export default {
  components: {
    schemas: {
      id: {
        type: 'integer',
        description: 'An id of an Book',
        example: '1',
      },
      Book: {
        type: 'object',
        properties: {
          books: {
            type: 'array',
            description: 'Book array',
            example: '[]',
          },
        },
      },
      BookInput: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            description: 'Book name',
            example: 'Book AYD2',
          },
          description: {
            type: 'string',
            description: 'Book description',
            example: 'AYD2',
          },
          price: {
            type: 'integer',
            description: 'Book price',
            example: 99,
          },
          author: {
            type: 'string',
            description: 'Book author',
            example: 'Desconocido',
          },
          image: {
            type: 'string',
            description: 'Imagen Libro',
            example: 'https://storage.googleapis.com/lab_ayd2/book/noimage.png',
          },
        },
      },
      Error: {
        type: 'object',
        properties: {
          message: {
            type: 'string',
          },
          internal_code: {
            type: 'string',
          },
        },
      },
    },
  },
}
