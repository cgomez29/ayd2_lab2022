import basicInfo from './basicInfo'
import servers from './servers'
import components from './components'
import book from './book'

export default {
  ...basicInfo,
  ...servers,
  ...components,
  ...book,
}
