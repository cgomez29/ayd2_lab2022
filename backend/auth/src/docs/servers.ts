export default {
  servers: [
    {
      url: 'http://localhost:4000/',
      description: 'Local server',
    },
    {
      url: 'http://32.54.56.32:4000/',
      description: 'DEV server',
    },
  ],
}
