import { Router } from 'express'
import { healthy, login } from '../controllers/auth.controller'

const router = Router()

router.get('/ping', healthy)
router.post('/login', login)

export default router
