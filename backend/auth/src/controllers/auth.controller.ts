import { Request, Response } from 'express'
import jwt, { SignOptions } from 'jsonwebtoken'
import fs from 'fs'

const JWT_SECRET = process.env.JWT_SECRET as string
const JWT_EXPIRES_IN = process.env.JWT_EXPIRES_IN as string

export const healthy = (req: Request, res: Response) => {
  try {
    global.log.info('Healthy')
    return res.status(200).json({ msg: 'true' })
  } catch (error) {
    if (error instanceof Error) {
      global.log.error(`Healthy: ${error.message}`)
      return res.status(500).json({ error: error.message })
    }
  }
}

export const login = (req: Request, res: Response) => {
  try {
    global.log.info(`${JWT_SECRET}`)
    global.log.info(`${JWT_EXPIRES_IN}`)
    const accessToken = createAccessToken(100, 'ayd2')

    return res.status(200).json({ accessToken })
  } catch (error) {
    if (error instanceof Error) {
      global.log.error(`${error.message}`)
    }
  }
}

const createAccessToken = (userId: number, username: string) => {
  const privateKey = fs.readFileSync(JWT_SECRET, 'utf8')

  const payload = {
    userId,
    username,
  }

  const i = 'AYD2' // Issuer
  const s = 'ayd2' // Subject
  const a = 'https://ayd2.com' // Audience

  // SIGNING OPTIONS
  const signOptions: SignOptions = {
    issuer: i,
    subject: s,
    audience: a,
    expiresIn: JWT_EXPIRES_IN,
    algorithm: 'RS256',
  }

  return jwt.sign(payload, privateKey, signOptions)
}
