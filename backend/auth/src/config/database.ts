import { Sequelize } from 'sequelize'

const { DB_NAME, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, NODE_ENV } = {
  DB_NAME: process.env.DB_NAME as string,
  DB_USERNAME: process.env.DB_USERNAME as string,
  DB_PASSWORD: process.env.DB_PASSWORD as string,
  DB_HOST: process.env.DB_HOST as string,
  DB_PORT: process.env.DB_PORT as string,
  NODE_ENV: process.env.NODE_ENV as string,
}

const sequelizaConnection = new Sequelize(DB_NAME, DB_USERNAME, DB_PASSWORD, {
  host: DB_HOST,
  port: Number(DB_PORT),
  dialect: 'postgres',
  logging: NODE_ENV === 'dev' ? console.log : false,
  define: {
    freezeTableName: true,
    timestamps: false,
  },
})

export default sequelizaConnection
