import Joi from 'joi'

const validationName = {
  'string.base': 'name is required',
  'string.empty': 'name is empty',
  'string.required': 'name is required',
}

export default Joi.object().keys({
  name: Joi.string().trim().required().messages(validationName),
})
