import { Response, Request, NextFunction } from 'express'
import model from '../models/validator'

const validate = (body: any) => model.validate(body)

const validator = (req: Request, res: Response, next: NextFunction) => {
  const { body } = req
  const validationResult = validate(body)

  if (validationResult.error) {
    global.log.error({ ...validationResult.error })
    return res.status(400).send({ ...validationResult.error })
  }
  next()
}

export default validator
