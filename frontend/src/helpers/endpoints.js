export const AUTH = {
  LOGIN: '/auth/login',
}

export const BOOK = {
  GET_BOOKS: '/book',
  POST_BOOK: '/book',
}
