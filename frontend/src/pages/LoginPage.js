import { useDispatch, useSelector } from 'react-redux'
import { useFormik } from 'formik'

import {
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  Typography,
} from '@mui/material'
import { Box } from '@mui/system'

import { loginWithUsernameAndPassword } from '../store/slices/auth/actions'

export const LoginPage = () => {
  const dispatch = useDispatch()

  const { isLoading } = useSelector(state => state.auth)

  const { handleSubmit } = useFormik({
    initialValues: {
      email: 'test',
      password: 'test',
    },
    onSubmit: values => {
      const { username, password } = { username: 'test', password: 'test' }
      dispatch(loginWithUsernameAndPassword(username, password))
    },
  })

  return (
    <Box sx={{ width: 450, padding: '10px 20px' }}>
      <Card>
        <CardContent>
          <form onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Typography variant="h1" component="h1">
                  Iniciar sesión
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <TextField fullWidth name="email" label="Email" />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  color="secondary"
                  size="large"
                  type="submit"
                  fullWidth
                  disabled={isLoading}
                >
                  Ingresar
                </Button>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Box>
  )
}
