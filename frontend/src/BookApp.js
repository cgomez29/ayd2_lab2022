import { ThemeProvider } from '@mui/material'
import { lightTheme } from './style/lightTheme'
import { AppRouter } from './routes/AppRouter'
import { BrowserRouter } from 'react-router-dom'

function BookApp() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={lightTheme}>
        <AppRouter />
      </ThemeProvider>
    </BrowserRouter>
  )
}

export default BookApp
