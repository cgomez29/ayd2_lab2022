import axios from 'axios'
import { getEnvVariables } from '../helpers/getEnvVariables'

const api = axios.create({
  baseURL: getEnvVariables().REACT_APP_SERVER,
  headers: {
    'Content-Type': 'application/json',
  },
})

export default api
