import api from '../../../api/api'
import { AUTH } from '../../../helpers/endpoints'
import { login, logout, startLoadingLogin } from './authSlice'

export const loginWithUsernameAndPassword =
  (email, password) => async dispatch => {
    dispatch(startLoadingLogin())

    const { data } = await api.post(AUTH.LOGIN, {
      email,
      password,
    })

    console.log(email, password)

    localStorage.setItem('token', data.accessToken)

    dispatch(login({ accessToken: data.accessToken, user: {} }))
  }

export const logoutRemoveToken = () => dispatch => {
  localStorage.removeItem('token')
  dispatch(logout())
}

export const loadUser = () => dispatch => {
  const token = localStorage.getItem('token')
  if (token) {
    dispatch(login({ accessToken: token, user: {} }))
  }
}
