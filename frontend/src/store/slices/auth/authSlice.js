import { createSlice } from '@reduxjs/toolkit'

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isLoading: false,
    user: {},
    isLogged: false,
  },
  reducers: {
    startLoadingLogin: state => {
      state.isLoading = true
    },
    login: (state, { payload }) => {
      state.isLoading = false
      state.user = payload
      state.isLogged = true
    },
    logout: state => {
      state.isLoading = false
      state.user = {}
      state.isLogged = false
    },
  },
})

export const { startLoadingLogin, login, logout } = authSlice.actions
