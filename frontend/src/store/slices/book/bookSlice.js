import { createSlice } from '@reduxjs/toolkit'
import api from '../../../api/api'
import { BOOK } from '../../../helpers/endpoints'

export const bookSlice = createSlice({
  name: 'book',
  initialState: {
    books: [],
    isLoading: false,
  },
  reducers: {
    startLoadingBooks: state => {
      state.isLoading = true
    },
    setBooks: (state, actions) => {
      state.isLoading = false
      state.books = actions.payload.books
    },
  },
})

export const { startLoadingBooks, setBooks } = bookSlice.actions

export const getBooks = () => async dispatch => {
  dispatch(startLoadingBooks())
  const { data } = await api.get(BOOK.GET_BOOKS)

  dispatch(setBooks({ books: data.books }))
}
