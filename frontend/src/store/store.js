import { configureStore } from '@reduxjs/toolkit'
import { bookSlice } from './slices/book/bookSlice'
import { authSlice } from './slices/auth/authSlice'

export const store = configureStore({
  reducer: {
    book: bookSlice.reducer,
    auth: authSlice.reducer,
  },
})
