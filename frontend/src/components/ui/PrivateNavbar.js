import { AppBar, Button, Toolbar, Typography } from '@mui/material'
import { Box } from '@mui/system'
import { useDispatch } from 'react-redux'
import { NavLink, useNavigate } from 'react-router-dom'
import { logoutRemoveToken } from '../../store/slices/auth/actions'

export const PrivateNavbar = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const navigateToLogOut = url => {
    navigate(url, { replace: true })
    dispatch(logoutRemoveToken())
  }

  return (
    <AppBar>
      <Toolbar>
        <NavLink
          to="/"
          style={{
            textDecoration: 'none',
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <Typography variant="h6">Book</Typography>
          <Typography variant="h6">|</Typography>
          <Typography sx={{ ml: 0.5 }}>Store</Typography>
        </NavLink>
        <Box flex={1} />
        <Box>
          <Button onClick={() => navigateToLogOut('/inicio')}>Salir</Button>
        </Box>
      </Toolbar>
    </AppBar>
  )
}
