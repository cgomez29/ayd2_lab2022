import { AppBar, Button, Toolbar, Typography } from '@mui/material'
import { Box } from '@mui/system'
import { NavLink } from 'react-router-dom'

export const Navbar = () => {
  return (
    <AppBar>
      <Toolbar>
        <NavLink
          to="/inicio"
          style={{
            textDecoration: 'none',
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <Typography variant="h6">Book</Typography>
          <Typography variant="h6">|</Typography>
          <Typography sx={{ ml: 0.5 }}>Store</Typography>
        </NavLink>
        <Box flex={1} />
        <Box>
          <NavLink to="/inicio" style={{ textDecoration: 'none' }}>
            <Button>Home</Button>
          </NavLink>
          <NavLink to="/inicio/login" style={{ textDecoration: 'none' }}>
            <Button>LogIn</Button>
          </NavLink>
        </Box>
        <Box flex={1} />
      </Toolbar>
    </AppBar>
  )
}
