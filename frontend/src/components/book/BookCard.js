import {
  Box,
  Card,
  CardActionArea,
  CardMedia,
  Grid,
  Typography,
} from '@mui/material'
import React from 'react'

export const BookCard = ({ book }) => {
  return (
    <Grid item xs={6} sm={4}>
      <Card>
        <CardActionArea>
          <CardMedia
            component="img"
            image={book.image}
            alt={book.description}
          />
        </CardActionArea>
      </Card>
      <Box sx={{ mt: 1, display: 'block' }}>
        <Typography fontWeight={700}>{book.name}</Typography>
      </Box>
    </Grid>
  )
}
