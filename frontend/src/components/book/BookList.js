import { Grid } from '@mui/material'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getBooks } from '../../store/slices/book/bookSlice'
import { Loading } from '../ui/Loading'
import { BookCard } from './BookCard'

export const BookList = () => {
  const dispatch = useDispatch()
  const { isLoading, books } = useSelector(state => state.book)

  useEffect(() => {
    dispatch(getBooks())
  }, [dispatch])

  if (isLoading) return <Loading />

  return (
    <Grid container spacing={4}>
      {books.map(book => (
        <BookCard key={book.id} book={book} />
      ))}
    </Grid>
  )
}
