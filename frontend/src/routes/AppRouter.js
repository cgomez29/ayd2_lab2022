import { Route, Routes } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Box } from '@mui/material'

import { BookList } from '../components/book/BookList'
import { Navbar } from '../components/ui/Navbar'
import { HomePage } from '../pages/HomePage'
import { PrivateRoutes } from './PrivateRoutes'
import { PublicRoutes } from './PublicRoutes'
import { PrivateNavbar } from '../components/ui/PrivateNavbar'
import { LoginPage } from '../pages/LoginPage'
//import { useEffect } from 'react'
//import { loadUser } from '../store/slices/auth/actions'

export const AppRouter = () => {
  const { isLogged } = useSelector(state => state.auth)
  //  const dispatch = useDispatch()

  //  useEffect(() => {
  //    dispatch(loadUser())
  //  }, [dispatch])

  return (
    <div
      style={{
        margin: '80px auto',
        maxWidth: '1440px',
        padding: '0px 30px',
      }}
    >
      <Routes>
        <Route
          path="/inicio/*"
          element={
            <PublicRoutes isAuthenticated={isLogged}>
              {/*<HomeRoutes />*/}
              <>
                <Navbar />
                <Box
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <Routes>
                    <Route path="/" element={<BookList />} />
                    <Route path="/login" element={<LoginPage />} />
                  </Routes>
                </Box>
              </>
            </PublicRoutes>
          }
        />
        <Route
          path="/*"
          element={
            <PrivateRoutes isAuthenticated={isLogged}>
              <>
                <PrivateNavbar />

                <Box>
                  <Routes>
                    <Route path="/" element={<HomePage />} />
                  </Routes>
                </Box>
              </>
            </PrivateRoutes>
          }
        />
      </Routes>
    </div>
  )
}
