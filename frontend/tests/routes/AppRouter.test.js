import { render, screen } from '@testing-library/react'
import { Provider } from 'react-redux'
import { MemoryRouter } from 'react-router-dom'
import { AppRouter } from '../../src/routes/AppRouter'
import { store } from '../../src/store/store'

const mockLoginWithUsernameAndPassword = jest.fn()

jest.mock(
  '../../src/store/slices/auth/actions',
  () => (email, password) => mockLoginWithUsernameAndPassword(email, password)
)

describe('Pruebas en <AppRouter />', () => {
  test('debe de mostrar el login correctamente', () => {
    const { container } = render(
      <MemoryRouter initialEntries={['/inicio/login']}>
        <Provider store={store}>
          <AppRouter />
        </Provider>
      </MemoryRouter>
    )

    expect(container).toMatchSnapshot()
  })
})
