import {
  bookSlice,
  startLoadingBooks,
} from '../../src/store/slices/book/bookSlice'

describe('Pruebas en bookSlice', () => {
  test('initial state valido', () => {
    expect(bookSlice.getInitialState()).toEqual({
      books: [],
      isLoading: false,
    })
  })

  test('startLoadingBooks', () => {
    let state = bookSlice.getInitialState()

    state = bookSlice.reducer(state, startLoadingBooks())
    expect(state.isLoading).toBeTruthy()
  })
})
