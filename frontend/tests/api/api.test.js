import api from '../../src/api/api'

describe('Test on api', () => {
  test('should have dafult settings', () => {
    expect(api.defaults.baseURL).toBe(process.env.REACT_APP_SERVER)
  })
})
