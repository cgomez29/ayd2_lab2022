describe('Pruebas en <LoginPage />', () => {
  it('La ruta final debe ser "/inicio"', () => {
    cy.visit('http://localhost:3000/inicio/login')

    cy.wait(500)
    cy.get('input[name="email"]').type('cgomez@gmail.com')

    cy.wait(500)
    cy.get('input[name="password"]').type('123456')

    cy.wait(500)
    cy.contains('button', /Ingresar/i).click()

    cy.wait(2000)

    cy.get('div').contains('div', /HomePage/i)

    cy.wait(1000)

    cy.contains('button', /Salir/i).click()

    cy.wait(1000)

    cy.url().should('include', '/inicio')
  })
})
